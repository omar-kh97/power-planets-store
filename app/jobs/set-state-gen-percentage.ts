// fetch states
// set generate percentage if not have been set yet

import { State } from "../core/models";
import { stateRepo } from "../core/repositories";



const handler = async () => {
    let states: State[] = await stateRepo.getStatesOverview(2020);
    let already_calculated = true;

    // if the percentage calculated already or not
    for (let i = 0; i < states.length; i++) {
        if (states[i].STGENPERC == null) {
            already_calculated = false
        }
    }

    if (already_calculated) {
        return
    }
    else {
        console.log('set-state-gen-percentage ...')
    }


    let sumStateGenration = states.map(state => state.STNGENAN).reduce((sumGen, stgenan) => {
        return sumGen + stgenan
    })



    let calculatedStates: State[] = states.map(state => ({
        ...state,
        STGENPERC: (100 * state.STNGENAN) / sumStateGenration
    }))


    stateRepo.updateGenPerc(calculatedStates)

}

const statePercenrate = { handler }
export default statePercenrate