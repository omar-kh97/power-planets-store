import { db } from '../../services';
import { helper } from '../../utils';
import { Planet } from '../models';


const TABLE = 'planet_year';

const getTopNPlanets = async (n: number, year: number, state?: string):Promise<Planet[]> => {
    let planets = await db.query<Planet>(
        `SELECT ID, PNAME, PSTATABB, LAT, LON, PLNGENAN FROM ${TABLE}
        WHERE 1=1
        AND YEAR = ${year}
        ${state ? `AND PSTATABB = '${state}'` : ''}
        ORDER BY PLNGENAN DESC LIMIT ${n}`
    );
    return helper.emptyOrRows(planets);
}

const getPlanet = (id: number): Promise<Planet> => {
    return db.queryOne<Planet>(
        `SELECT * FROM ${TABLE} WHERE ID = ${id}`
    );
     
}

const planetRepo = { getTopNPlanets, getPlanet }

export default planetRepo