import { db } from "../../services"
import { helper } from "../../utils"
import { State } from "../models";

const TABLE = 'state_year'
const getStatesOverview = async (year: number = 2020): Promise<State[]> => {
    let statesOverview = await db.query<State>(
        `SELECT ID, PSTATABB, FIPSST, STGENPERC, STNAMEPCAP, STNGENAN, STGENPERC
        FROM ${TABLE}
        WHERE YEAR = ${year}
        `
    )
    return helper.emptyOrRows(statesOverview);
}

const getStateByCode = async (state: string): Promise<State> => {
    return db.queryOne<State>(
        `SELECT * FROM ${TABLE} WHERE PSTATABB = '${state}'`
    )
}

const updateGenPerc = async (states: State[]) => {
    db.query(`
    INSERT INTO ${TABLE} (ID,STGENPERC) VALUES 
    ${states.map((state) => `(${state.ID}, ${state.STGENPERC})`)}
    ON DUPLICATE KEY UPDATE STGENPERC=VALUES(STGENPERC);`)
}

const stateRepo = { getStatesOverview, updateGenPerc, getStateByCode }
export default stateRepo