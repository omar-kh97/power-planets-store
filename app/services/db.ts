import { RowDataPacket } from "mysql2"
import pool from './pool'

const query = async <T extends RowDataPacket>(sql: string, params?: any[]): Promise<T[]> => {
  try {
    const connection = await pool.getConnection();
    let startTimeStamp = new Date().getTime()
    const [results,] = await connection.query<T[]>(sql, params);
    let endTimeStamp = new Date().getTime()
    console.log(endTimeStamp - startTimeStamp)
    connection.release()
    return results;
  }
  catch (e) {
    console.error(e)
    return []
  }

}

const queryOne = async <T extends RowDataPacket>(sql: string, params?: any[]): Promise<T> => {
  try {
    const connection = await pool.getConnection();
    const [results,] = await connection.query<T[]>(sql, params);
    connection.release()
    return results[0];
  }
  catch (e) {
    console.error(e)
    return null as any
  }
}

const db = {
  query,
  queryOne
}
export default db
