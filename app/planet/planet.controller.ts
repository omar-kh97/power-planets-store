import { Router, Request, Response } from 'express';
import { getPlanet} from './planet.service';

const router: Router = Router();


router.get('/:id', async (req: Request, res: Response) => {
	let id = Number.parseInt(req.params.id)
	res.send(await getPlanet(id))
});

export const planetRouter: Router = router;