import { planetRepo } from "../core/repositories"


const getPlanet = (id: number) => {
    if(!id) {
        throw new Error('Planet id cant be null')
    }
    return planetRepo.getPlanet(id)
}

export {getPlanet}