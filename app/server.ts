import express from 'express';
import startup from './startup'
import {TopPlanetsRouter, planetRouter} from './modules';
// import swaggerUi from 'swagger-ui-express';
// import swaggerJsdoc from 'swagger-jsdoc';
import cors from 'cors'

// const options = {
//     definition: {
//       openapi: '3.0.0',
//       info: {
//         title: 'Hello World',
//         version: '1.0.0',
//       },
//     },
//     apis: ['./top-planets/top-planets.controller.ts'], 
//   };
// const swaggerSpec = swaggerJsdoc(options);

const app: express.Application = express();
const port: number = Number(process.env.PORT) || 3000;
startup()

app.use(cors())
app.use('/api/topn', TopPlanetsRouter);
app.use('/api/planet', planetRouter)
// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/`);
});

