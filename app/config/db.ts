import { ConnectionOptions } from "mysql2";
import 'dotenv/config'

const env = process.env
const db:ConnectionOptions = {
    connectionLimit : 3,
    host: env.DB_HOST,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB,
    decimalNumbers: true
};

export default db
