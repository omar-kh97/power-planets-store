const emptyOrRows = (rows: any[]) => {
    if (!rows) {
        return [];
    }
    return rows;
}

const helper = {emptyOrRows}
export default helper