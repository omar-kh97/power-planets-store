import { Router, Request, Response } from 'express';
import { getTopPlanetsOnState, getTopPlanetsOnStates } from './top-planets.service';

const router: Router = Router();

/**
 * @openapi
 * /:
 *   get:
 *     title: Top N Planets
 *     description: Get Top annual power generation planets in all united states AND get states overview
 *     parameters: 
 *       TopN:
 *         name: top
 *         in: query
 *         description: Top n power planets number
 *         required: true
 *         schema:
 *				 	 type: integer
 *					 formate: int32
 *     responses:
 *       200:
 *         description: Returns a mysterious string.
 */
router.get('/', async (req: Request, res: Response) => {
	let topPlanets = Number.parseInt(<string>req.query.top)
	res.send(await getTopPlanetsOnStates(topPlanets))
});

router.get('/:state', async (req: Request, res: Response) => {
	let topPlanets = Number.parseInt(<string>req.query.top)
	let state = req.params.state
	res.send(await getTopPlanetsOnState(topPlanets, state))
});

export const TopPlanetsRouter: Router = router;