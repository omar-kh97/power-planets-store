import { Planet, State } from "../core/models"
import { planetRepo, stateRepo } from "../core/repositories"

const TOP_DEFAULT = 10
const DEFAULT_YEAR = 2020
const getTopPlanetsOnStates = async (planetsNumber: number, year?: number): Promise<{ planets: Planet[], states: State[] }> => {
    if (!planetsNumber || typeof planetsNumber != 'number' || planetsNumber < 0) {
        planetsNumber = TOP_DEFAULT
    }

    if (!year) {
        year = DEFAULT_YEAR
    }
    let planets: Planet[]
    let states: State[]
    [planets, states] = await Promise.all<any>([planetRepo.getTopNPlanets(planetsNumber, year), stateRepo.getStatesOverview(2020)])
    return {
        planets,
        states
    }
}

const getTopPlanetsOnState = async (planetsNumber: number, state: string, year?: number): Promise<{ planets: Planet[], stateInfo: State }> => {

    if (!state) {
        throw new Error('State should be selected')
    }
    if (!planetsNumber || typeof planetsNumber != 'number' || planetsNumber < 0) {
        planetsNumber = TOP_DEFAULT
    }

    if (!year) {
        year = DEFAULT_YEAR
    }

    let planets: Planet[]
    let stateInfo: State
    [planets, stateInfo] = await Promise.all<any>([planetRepo.getTopNPlanets(planetsNumber, year, state), stateRepo.getStateByCode(state)])
    return {
        planets,
        stateInfo
    }

}

export { getTopPlanetsOnStates, getTopPlanetsOnState }