# Top-plants service
Porject that supply data for power-plants on US

## Deployment
The app deployed on Heroku, 
https://power-planet-store.herokuapp.com/

App endpoints: 

name: TopN <br>
description: top n power plant among united states <br>
METHOD: GET <br>
request endpoint: `https://power-planet-store.herokuapp.com/api/topn?top={n}` <br>
response: array of power plants info and states info <br>
example : `https://power-planet-store.herokuapp.com/api/topn?top=10` <br>
shows top 10 plants <br>

----
name: TopNOnState <br>
description: top n power plant on specific state <br>
METHOD: GET <br>
request endpoint: `https://power-planet-store.herokuapp.com/api/topn/{state}/topn?top=5` <br>
response: array of power plants and state info <br>
example : `https://power-planet-store.herokuapp.com/api/topn/WA/topn?top=5` <br>
shows top 10 plants in washington <br>

----

name: getPlant <br>
description: get specific power plant info <br>
METHOD: GET <br>
request endpoint: `https://power-planet-store.herokuapp.com/api/planet/{id}` <br>
response: array of power plants and state info <br>
example : `https://power-planet-store.herokuapp.com/api/planet/13` <br>
get power plant with id of 13 <br>

---

## API performence
Results got within 200ms topn API 😄, <br>
This performence done by some work on: <br>

### On data base level
2 indexes built on planet_year table <br>
Index on PLNGENAN field (net generation) <br>
Index on PSTATABB and PLNGENAN field (state, net generation) <br>

Those indexes improve our queries performence

### On server-side level
<b>Promise.all()</b> have been used on service-level, <br>
And <b>Connection pool</b> for connection speed <br>

## Local development
in order to get started:  <br>
- clone the project, and then run <br>

```
npm i
npm run tsc
npm run serve
```
the server will start listen on port 3000

- In order to connect to my db, create `.env` file in the project root:

```
DB_HOST="power-plants1.cvhnwtbl8ojy.us-east-1.rds.amazonaws.com"
DB_USER="root"
DB_PASSWORD="Asd!2345"
DB="power-planet"
```

## Features mention
- start-up functionlity was Developed, the functionality run calcualtion 
for all states net power generation, and set the generation percentage for each
of them, this calculation runs just one time on (if the percentage not calculated
for any state).